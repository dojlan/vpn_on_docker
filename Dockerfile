RUN yum -y update 
RUN yum -y install epel-release
RUN yum -y install openvpn
CMD [ "openvpn", "--config", "/etc/openvpn/client.ovpn" ]